package Debian::PkgJs::Version;

use Exporter 'import';

our $VERSION = '0.14.19';
our @EXPORT = qw($VERSION);

